# Grocery Store
By Sriraam Nadarajah and Andrew Rokakis

## Information
- The project needs to be run from the folder containing the `.git` folder (the folder with this readme in it) in order to properly access the resources file.
- A demo of how `SqlReader` would work can be found in `YogurtImporter.java`. Assuming the required tables exist, uncommenting the code and adding the correct imports is enough to make it work. We tested it with a proper table and it worked just the same as the csv reader.
