package project.Comparer;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import project.Item.Item;
import project.Item.UnweightedItem.UnweightedItem;

public class PriceComparer {
    @Test
    public void priceComparerAscending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 2),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 3)
        );

        Comparator<Item> comparator = new PriceComparerAscending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", 1, items.get(0).getPrice(), 0.001);
        assertEquals("Sorting failed at index 1", 2, items.get(1).getPrice(), 0.001);
        assertEquals("Sorting failed at index 2", 3, items.get(2).getPrice(), 0.001);
    }

    @Test
    public void priceComparerDescending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 2),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 3)
        );

        Comparator<Item> comparator = new PriceComparerDescending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", 3, items.get(0).getPrice(), 0.001);
        assertEquals("Sorting failed at index 1", 2, items.get(1).getPrice(), 0.001);
        assertEquals("Sorting failed at index 2", 1, items.get(2).getPrice(), 0.001);
    }
}
