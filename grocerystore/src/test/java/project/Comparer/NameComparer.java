package project.Comparer;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import project.Item.Item;
import project.Item.UnweightedItem.UnweightedItem;

public class NameComparer {
    @Test
    public void nameComparerAscending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Banana", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Apple", "12-Dec-23", 1, 12, 1)
        );

        Comparator<Item> comparator = new NameComparerAscending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", "Apple", items.get(0).getName());
        assertEquals("Sorting failed at index 1", "Banana", items.get(1).getName());
        assertEquals("Sorting failed at index 2", "Carrot", items.get(2).getName());
    }

    @Test
    public void nameComparerDescending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Banana", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Apple", "12-Dec-23", 1, 12, 1)
        );

        Comparator<Item> comparator = new NameComparerDescending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", "Carrot", items.get(0).getName());
        assertEquals("Sorting failed at index 1", "Banana", items.get(1).getName());
        assertEquals("Sorting failed at index 2", "Apple", items.get(2).getName());
    }
}
