package project.Comparer;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import project.Item.Item;
import project.Item.UnweightedItem.UnweightedItem;

public class QuantityComparer {
    @Test
    public void quantityComparerAscending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 2, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 1, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 3, 1)
        );

        Comparator<Item> comparator = new QuantityComparerAscending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", 1, items.get(0).getQuantity());
        assertEquals("Sorting failed at index 1", 2, items.get(1).getQuantity());
        assertEquals("Sorting failed at index 2", 3, items.get(2).getQuantity());
    }

    @Test
    public void quantityComparerDescending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 2, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 1, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 3, 1)
        );

        Comparator<Item> comparator = new QuantityComparerDescending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", 3, items.get(0).getQuantity());
        assertEquals("Sorting failed at index 1", 2, items.get(1).getQuantity());
        assertEquals("Sorting failed at index 2", 1, items.get(2).getQuantity());
    }
}
