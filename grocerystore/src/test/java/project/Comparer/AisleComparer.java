package project.Comparer;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import project.Item.Item;
import project.Item.UnweightedItem.UnweightedItem;

public class AisleComparer {
    @Test
    public void aisleComparerAscending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Carrot", "12-Dec-23", 2, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 3, 12, 1)
        );

        Comparator<Item> comparator = new AisleComparerAscending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", 1, items.get(0).getAisle());
        assertEquals("Sorting failed at index 1", 2, items.get(1).getAisle());
        assertEquals("Sorting failed at index 2", 3, items.get(2).getAisle());
    }

    @Test
    public void aisleComparerDescending() {
        List<UnweightedItem> items = Arrays.asList(
            new UnweightedItem(1, "Carrot", "12-Dec-23", 2, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 1, 12, 1),
            new UnweightedItem(1, "Carrot", "12-Dec-23", 3, 12, 1)
        );

        Comparator<Item> comparator = new AisleComparerDescending();
        Collections.sort(items, comparator);

        assertEquals("Sorting failed at index 0", 3, items.get(0).getAisle());
        assertEquals("Sorting failed at index 1", 2, items.get(1).getAisle());
        assertEquals("Sorting failed at index 2", 1, items.get(2).getAisle());
    }
}
