package project.Discount;

import static org.junit.Assert.*;
import org.junit.Test;

import project.Item.WeightedItem.WeightedItem;
import project.Item.Item;
import project.Item.UnweightedItem.UnweightedItem;

public class DiscountTest {
    
    @Test
    public void coupon_getDiscountedPrice_weightedItem() {
        Item i = new WeightedItem(104, "Carrot", "12-Dec-23", 2, 12, 5, 2);
        Discount d = new Coupon(0, 25, 104);
        assertEquals("Discounted price does not reflect coupon", 7.5, d.getDiscountedPrice(i.getPrice()), 0.001);
    }

    @Test
    public void coupon_getDiscountedPrice_unweightedItem() {
        Item i = new UnweightedItem(104, "Carrot", "12-Dec-23", 2, 12, 10);
        Discount d = new Coupon(0, 25, 104);
        assertEquals("Discounted price does not reflect coupon", 7.5, d.getDiscountedPrice(i.getPrice()), 0.001);
    }

    @Test
    public void coupon_onItem_getPrice() {
        Item i = new UnweightedItem(104, "Carrot", "12-Dec-23", 2, 12, 10);
        Discount d = new Coupon(0, 25, 104);
        i.setDiscount(d);
        assertEquals("Discounted price was not properly calculated from item", 7.5, i.getPrice(), 0.001);
    }

    @Test
    public void coupon_onItem_getRawPrice() {
        Item i = new UnweightedItem(104, "Carrot", "12-Dec-23", 2, 12, 10);
        Discount d = new Coupon(0, 25, 104);
        i.setDiscount(d);
        assertEquals("Raw price was not properly returned for discounted item", 10, i.getRawPrice(), 0.001);
    }
}
