package project.Item;

import static org.junit.Assert.*;
import org.junit.Test;

import project.Item.UnweightedItem.UnweightedItem;


public class ItemTest {
    
    @Test
    public void ItemConstructorValid() {
        new UnweightedItem(104,"Carrot","12-Dec-23",2,12, 1);
    }

    @Test
    public void ItemConstructorNegativeId() {
        try {
            new UnweightedItem (-2,"Carrot","12-Dec-23",2,12, 1);
            fail("Item accepted a negative id");
        }
        catch(IllegalArgumentException e){}
    }

    @Test
    public void ItemConstructorNegativeAisle() {
        try {
            new UnweightedItem (2,"Carrot","12-Dec-23",-2,12, 1);
            fail("Item accepted a negative aisle");
        }
        catch(IllegalArgumentException e){}
    }

    @Test
    public void ItemConstructorNegativeQuantity() {
        try {
            new UnweightedItem (-2,"Carrot","12-Dec-23",2,-12, 1);
            fail("Item accepted a negative quantity");
        }
        catch(IllegalArgumentException e){}
    }


}
