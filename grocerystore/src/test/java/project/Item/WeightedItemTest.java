package project.Item;

import static org.junit.Assert.*;
import org.junit.Test;

import project.Item.WeightedItem.WeightedItem;

public class WeightedItemTest {
    @Test
    public void WeightedItem_NegativePrice_ValidWeight() {
        try {
            new WeightedItem(101, "Tuna Filet", "16-Nov-23", 1, 12, -1, 1);
            fail("WeightedItem accepted a negative price");
        }
        catch(Exception _) { }
    }

    @Test
    public void WeightedItem_ValidPrice_NegativeWeight() {
        try {
            new WeightedItem(101, "Tuna Filet", "16-Nov-23", 1, 12, 1, -1);
            fail("WeightedItem accepted a negative weight");
        }
        catch(Exception _) { }
    }

    @Test
    public void WeightedItem_ZeroPrice_ValidWeight() {
        try {
            new WeightedItem(101, "Tuna Filet", "16-Nov-23", 1, 12, 0, 1);
            fail("WeightedItem accepted a zero price");
        }
        catch(Exception _) { }
    }

    @Test
    public void WeightedItem_ValidPrice_ZeroWeight() {
        try {
            new WeightedItem(101, "Tuna Filet", "16-Nov-23", 1, 12, 1, 0);
            fail("WeightedItem accepted a zero weight");
        }
        catch(Exception _) { }
    }
}
