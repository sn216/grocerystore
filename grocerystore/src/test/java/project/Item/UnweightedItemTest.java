package project.Item;

import static org.junit.Assert.*;
import org.junit.Test;

import project.Item.UnweightedItem.UnweightedItem;

public class UnweightedItemTest {
    
    @Test
    public void UnweightedItemItemConstructorValid() {
        new UnweightedItem(104,"Carrot","12-Dec-23",2,12,24);
    }

    @Test
    public void UnweightedItemItemConstructorNegativePrice() {
        try {
            new UnweightedItem(104,"Carrot","12-Dec-23",2,12,-23);
            fail("Item accepted a negative price");
        }
        catch(IllegalArgumentException e){}
    }

     @Test
    public void UnweightedItemItemConstructor0Price() {
        try {
            new UnweightedItem(104,"Carrot","12-Dec-23",2,12,0);
        }
        catch(IllegalArgumentException e){}
    }


}
