package project.Comparer;

import java.util.Comparator;

import project.Item.Item;

public class PriceComparerAscending implements Comparator<Item> {

    /**
     * Compares two items based on their price.
     * @param one The first item.
     * @param two The second item.
     * @return A negative integer, zero, or a positive integer if the first item is less than, equal to, or greater than 
     * the second item, respectively, based on their price.
     */
    @Override
    public int compare (Item one, Item two){
        return  (int)(one.getPrice() - two.getPrice()) ;
    }
}
