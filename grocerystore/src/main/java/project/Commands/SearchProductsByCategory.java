package project.Commands;

import java.util.ArrayList;
import java.util.List;

import project.Item.Item;
import project.Util.GetInput;
import project.Util.Sorter;
import project.Util.Validation;

/**
 * Represents a command that can be used to search for all products in a given category.
 */
public class SearchProductsByCategory extends Command {

    private List<Item> items;

    public SearchProductsByCategory(List<Item> items) {
        this.items = items;
    }

    @Override
    public String getCode() { return "sc"; }

    @Override
    public String getDescription() { return "Search items by category"; }

    @Override
    public void run() {
        System.out.println("Enter the category to search:");
        System.out.println("Here is a list of the categories: Bread, Cheese, Cookie, Fish, Milk, Pasta, Produce, Shellfish, Steak, and Yogurt");

        String category = GetInput.getCategoryInput();

        List<Item> categoryItems = new ArrayList<>();
        for (Item item : items) {
            if (Validation.isInstanceOfCategory(item, category)) {
                categoryItems.add(item);
            }
        }

        if (categoryItems.isEmpty()) {
            System.out.println("No items found for the specified category.");
            return;
        }

        List<Item> sortedItems = Sorter.promptNewSortedItemList(categoryItems);

        System.out.println("\nHere are all the items:");
        System.out.println(Item.getItemTableHeading());
        for (Item item : sortedItems) {
            System.out.println(item);
        }
    }
}
