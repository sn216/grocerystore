package project.Commands;

import java.util.Iterator;
import java.util.List;

import project.Item.Item;
import project.Util.GetInput;

/**
 * Represents a command that can be used to delete a product from the store.
 */
public class DeleteProduct extends Command {

    private List<Item> items;

    public DeleteProduct(List<Item> items) {
        this.items = items;
    }

    @Override
    public String getCode() { return "dp"; }

    @Override
    public String getDescription() { return "Delete product"; }
    
    /**
     * Prompts the user to enter the ID of the item to be deleted and removes the item from the list of all items.
     * The method continuously asks the user for input until a non-negative integer is provided as the item ID.
     * If an item with the specified ID is found, it is removed from the list, and a success message is printed.
     * If no item is found with the given ID, a corresponding message is printed.
     */
    @Override
    public void run() {
        int productId;

        while (true) {
            System.out.println("Enter the ID of the item to delete:");
            productId = GetInput.getIntInput();

            if (productId >= 0) {
                break;
            } 
            else {
                System.out.println("Invalid input. Please enter a non-negative integer.");
            }
        }

        boolean itemFound = false;
        Iterator<Item> iterator = items.iterator();

        while (iterator.hasNext()) {
            Item item = iterator.next();
            if (item.getId() == productId) {
                iterator.remove();
                System.out.println("Item with ID " + productId + " deleted successfully.");
                itemFound = true;
                break;
            }
        }

        if (!itemFound) {
            System.out.println("Item with ID " + productId + " not found.");
        }
    }
}
