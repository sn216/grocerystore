package project.Commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import project.Discount.Discount;
import project.Item.Item;

/**
 * Represents a command that can be used to display all available discounts.
 */
public class ViewAllDiscounts extends Command {

    private List<Item> items;

    public ViewAllDiscounts(List<Item> items) {
        this.items = items;
    }

    @Override
    public String getCode() { return "vd"; }

    @Override
    public String getDescription() { return "View all discounts"; }

    @Override
    public void run() {

        List<Discount> discounts = new ArrayList<>();
        Iterator<Item> itemIterator = items.iterator();

        while (itemIterator.hasNext()) {
            Item item = itemIterator.next();
            if (item.hasDiscount()) {
                discounts.add(item.getDiscount());
            }
        }

        if (discounts.isEmpty()) {
            System.out.println("No discounts found.");
            return;
        }

        System.out.println("\nHere are all the discounts:");
        System.out.printf("%-6s%-12s%-12s%-8s\n", "Id", "Type", "Discount", "Item Id");
        for (Discount discount : discounts) {
            System.out.println(discount);
        }
    }
}
