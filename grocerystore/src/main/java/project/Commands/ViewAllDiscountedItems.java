package project.Commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import project.Item.Item;
import project.Util.Sorter;

/**
 * Represents a command that can be used to display all items that have a discount.
 */
public class ViewAllDiscountedItems extends Command {

    private List<Item> items;

    public ViewAllDiscountedItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String getCode() { return "vdp"; }

    @Override
    public String getDescription() { return "View all discounted products"; }

    @Override
    public void run() {
        List<Item> discountedItems = new ArrayList<>();
        Iterator<Item> itemIterator = items.iterator();

        while (itemIterator.hasNext()) {
            Item item = itemIterator.next();
            if (item.hasDiscount()) {
                discountedItems.add(item);
            }
        }

        if (discountedItems.isEmpty()) {
            System.out.println("No discounted items found.");
            return;
        }

        List<Item> sortedItems = Sorter.promptNewSortedItemList(discountedItems);

        System.out.println("\nHere are all the items with discounts:");
        System.out.println(Item.getItemTableHeading());
        for (Item item : sortedItems) {
            System.out.println(item);
        }
    }
}
