package project.Commands;

import java.util.List;

import project.Item.Item;
import project.Util.Sorter;

/**
 * Represents a command that can be used to display every item in the store.
 */
public class ViewAllItems extends Command {

    private List<Item> items;

    public ViewAllItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String getCode() { return "vp"; }

    @Override
    public String getDescription() { return "View all products"; }

    @Override
    public void run() {
        if (items.isEmpty()) {
            System.out.println("No items found.");
            return;
        }

        List<Item> sortedItems = Sorter.promptNewSortedItemList(items);

        System.out.println("\nHere are all the items:");
        System.out.println(Item.getItemTableHeading());
        for (Item item : sortedItems) {
            System.out.println(item);
        }
    }
}
