package project.Commands;

public abstract class Command {
    /**
     * Returns a command code that when inputted into the console, will run this command.
     * @return The command's code.
     */
    public abstract String getCode();

    /**
     * The command description that will be displayed in the console.
     * @return Command description
     */
    public abstract String getDescription();

    /**
     * When called, the command will be run, prompting all user input that it might need.
     */
    public abstract void run();

    @Override
    public String toString() {
        return "(" + this.getCode() + ") " + this.getDescription();
    }
}
