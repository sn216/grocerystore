package project.Commands;

import java.util.List;

import project.Item.Item;
import project.Item.UnweightedItem.Bread;
import project.Item.UnweightedItem.Cheese;
import project.Item.UnweightedItem.Cookie;
import project.Item.UnweightedItem.Milk;
import project.Item.UnweightedItem.Pasta;
import project.Item.UnweightedItem.Yogurt;
import project.Item.WeightedItem.Fish;
import project.Item.WeightedItem.Produce;
import project.Item.WeightedItem.Shellfish;
import project.Item.WeightedItem.Steak;
import project.Util.GetInput;
import project.Util.Validation;

/**
 * Represents a command that can be used to add a product to the store.
 */
public class AddProduct extends Command {

    private List<Item> items;

    public AddProduct(List<Item> items) {
        this.items = items;
    }
    
    @Override
    public String getCode() { return "ap"; }

    @Override
    public String getDescription() { return "Add a product"; }

    @Override
    public void run() {
        System.out.println("Enter the category of the new product:");
        System.out.println("Categories: Bread, Cheese, Cookie, Fish, Milk, Pasta, Produce, Shellfish, Steak, Yogurt");
        String category = GetInput.getCategoryInput();
    
        switch (category.toLowerCase()) {
            case "bread":
                addBread();
                break;
            case "cheese":
                addCheese();
                break;
            case "cookie":
                addCookie();
                break;
            case "milk":
                addMilk();
                break;
            case "pasta":
                addPasta();
                break;
            case "yogurt":
                addYogurt();
                break;
            case "fish":
                addFish();
                break;
            case "produce":
                addProduce();
                break;
            case "shellfish":
                addShellfish();
                break;
            case "steak":
                addSteak();
                break;
            default:
                System.out.println("Invalid category. Please enter a valid category.");
        }
    }

    /**
     * Retrieves the next available item ID.
     * @return The next available item ID.
     */
    public int getNextId() {
        if (!items.isEmpty()) {
            return items.get(items.size() - 1).getId() + 1;
        } 
        else {
            return 1;
        }
    }

    /**
     * Adds a new Bread item to the inventory.
     * Asks the user to input information for the new Bread item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addBread() {
        try {
            System.out.println("Enter the name of the new Bread:");
            String name = GetInput.getStringInput();

            System.out.println("Enter the expiration date of the new Bread (format: dd-MMM-yy):");
            String expireDate = GetInput.getStringInput(); // Validate date format if needed

            System.out.println("Enter the quantity of the new Bread:");
            int quantity = GetInput.getIntInput();

            System.out.println("Enter the price of the new Bread:");
            double price = GetInput.getDoubleInput();

            System.out.println("Is the new Bread whole wheat? (true/false):");
            boolean wholeWheat = GetInput.getBooleanInput();

            System.out.println("Is the new Bread sliced? (true/false):");
            boolean sliced = GetInput.getBooleanInput();

            Bread newBread = new Bread(getNextId(), name, expireDate, Validation.getAisleByCategory("BREAD"), quantity, price, wholeWheat, sliced);

            items.add(newBread);

            System.out.println("New Bread added successfully.");
        } 
        catch (Exception e) {
            System.out.println("Error creating new Bread. Please check your input.");
        }
    }

    /**
     * Adds a new Cheese item to the inventory.
     * Asks the user to input information for the new Cheese item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addCheese() {
        try {
            System.out.println("Enter the name of the new Cheese:");
            String name = GetInput.getStringInput();
        
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Cheese:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price of the new Cheese:");
            double price = GetInput.getDoubleInput();
        
            System.out.println("Enter the source of the new Cheese:");
            String source = GetInput.getStringInput();
        
            System.out.println("Is the new Cheese vegan? (true/false):");
            boolean isVegan = GetInput.getBooleanInput();
        
            System.out.println("Enter the time aged for the new Cheese:");
            double timeAged = GetInput.getDoubleInput();
        
            Cheese newCheese = new Cheese(getNextId(), name, expireDate, Validation.getAisleByCategory("CHEESE"), quantity, price, source, isVegan, timeAged);
        
            items.add(newCheese);
        
            System.out.println("New Cheese added successfully.");
        }
        catch (Exception e) {
            System.out.println("Error creating new Cheese. Please check your input.");
        }
    }
    
    /**
     * Adds a new Cookie item to the inventory.
     * Asks the user to input information for the new Cookie item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addCookie() {
        try{
            System.out.println("Enter the name of the new Cookie:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Cookie (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Cookie:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price of the new Cookie:");
            double price = GetInput.getDoubleInput();
        
            System.out.println("Enter the ingredient of the new Cookie:");
            String ingredient = GetInput.getStringInput();
        
            Cookie newCookie = new Cookie(getNextId(), name, expireDate, Validation.getAisleByCategory("COOKIE"), quantity, price, ingredient);
        
            items.add(newCookie);
        
            System.out.println("New Cookie added successfully.");
        }
        catch (Exception e) {
            System.out.println("Error creating new Cookie. Please check your input.");
        }
    }

    /**
     * Adds a new Milk item to the inventory.
     * Asks the user to input information for the new Milk item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addMilk() {
        try{
            System.out.println("Enter the name of the new Milk:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Milk (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Milk:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price of the new Milk:");
            double price = GetInput.getDoubleInput();
        
            System.out.println("Enter the source of the new Milk:");
            String source = GetInput.getStringInput();
        
            System.out.println("Is the new Milk vegan? (true/false):");
            boolean isVegan = GetInput.getBooleanInput();
        
            System.out.println("Enter the percentage of the new Milk:");
            double percentage = GetInput.getDoubleInput();
        
            System.out.println("Enter the volume of the new Milk:");
            double volume = GetInput.getDoubleInput();
        
            System.out.println("Enter the packaging of the new Milk:");
            String packaging = GetInput.getStringInput();
        
            Milk newMilk = new Milk(getNextId(), name, expireDate, Validation.getAisleByCategory("MILK"), quantity, price, source, isVegan, percentage, volume, packaging);
        
            items.add(newMilk);
        
            System.out.println("New Milk added successfully.");
        }
        catch (Exception e){
            System.out.println("Error creating new Milk. Please check your input.");
        }
    }
    
    /**
     * Adds a new Pasta item to the inventory.
     * Asks the user to input information for the new Pasta item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addPasta() {
        try{
            System.out.println("Enter the name of the new Pasta:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Pasta (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Pasta:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price of the new Pasta:");
            double price = GetInput.getDoubleInput();
        
            System.out.println("Is the new Pasta whole wheat? (true/false):");
            boolean wholeWheat = GetInput.getBooleanInput();
        
            System.out.println("Enter the type of the new Pasta:");
            String type = GetInput.getStringInput();
        
            Pasta newPasta = new Pasta(getNextId(), name, expireDate, Validation.getAisleByCategory("PASTA"), quantity, price, wholeWheat, type);
        
            items.add(newPasta);
        
            System.out.println("New Pasta added successfully.");
        }
        catch (Exception e){
            System.out.println("Error creating new Pasta. Please check your input.");
        }
    }
    
    /**
     * Adds a new Yogurt item to the inventory.
     * Asks the user to input information for the new Yogurt item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addYogurt() {
        try{
            System.out.println("Enter the name of the new Yogurt:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Yogurt (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Yogurt:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price of the new Yogurt:");
            double price = GetInput.getDoubleInput();
        
            System.out.println("Enter the source of the new Yogurt:");
            String source = GetInput.getStringInput();
        
            System.out.println("Is the new Yogurt vegan? (true/false):");
            boolean isVegan = GetInput.getBooleanInput();
        
            System.out.println("Enter the flavor of the new Yogurt:");
            String flavor = GetInput.getStringInput();
        
            Yogurt newYogurt = new Yogurt(getNextId(), name, expireDate, Validation.getAisleByCategory("YOGURT"), quantity, price, source, isVegan, flavor);
        
            items.add(newYogurt);
        
            System.out.println("New Yogurt added successfully.");
        }
        catch (Exception e){
            System.out.println("Error creating Yogurt. Please check your input.");
        }
    }

    /**
     * Adds a new Fish item to the inventory.
     * Asks the user to input information for the new Fish item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addFish() {
        try{
            System.out.println("Enter the name of the new Fish:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Fish (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Fish:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price per pound of the new Fish:");
            double pricePerPound = GetInput.getDoubleInput();
        
            System.out.println("Enter the weight of the new Fish:");
            double weight = GetInput.getDoubleInput();
        
            System.out.println("Enter the origin of the new Fish:");
            String origin = GetInput.getStringInput();
        
            System.out.println("Does the new Fish have bones? (true/false):");
            boolean hasBones = GetInput.getBooleanInput();
        
            System.out.println("Enter the type of the new Fish:");
            String type = GetInput.getStringInput();
        
            System.out.println("Enter the cut of the new Fish:");
            String cut = GetInput.getStringInput();
        
            System.out.println("Does the new Fish have scales? (true/false):");
            boolean hasScales = GetInput.getBooleanInput();
        
            Fish newFish = new Fish(getNextId(), name, expireDate, Validation.getAisleByCategory("FISH"), quantity, pricePerPound, weight, origin, hasBones, type, cut, hasScales);
        
            items.add(newFish);
        
            System.out.println("New Fish added successfully.");
        }
        catch (Exception E){
            System.out.println("Error creating Fish. Please check your input.");
        }
    }

    /**
     * Adds a new Produce item to the inventory.
     * Asks the user to input information for the new Produce item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addProduce() {
        try{
            System.out.println("Enter the name of the new Produce:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Produce (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Produce:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price per pound of the new Produce:");
            double pricePerPound = GetInput.getDoubleInput();
        
            System.out.println("Enter the weight of the new Produce:");
            double weight = GetInput.getDoubleInput();
        
            System.out.println("Enter the origin of the new Produce:");
            String origin = GetInput.getStringInput();
        
            System.out.println("Does the new Produce have seeds? (true/false):");
            boolean hasSeeds = GetInput.getBooleanInput();
        
            Produce newProduce = new Produce(getNextId(), name, expireDate, Validation.getAisleByCategory("PRODUCE"), quantity, pricePerPound, weight, origin, hasSeeds);
        
            items.add(newProduce);
        
            System.out.println("New Produce added successfully.");
        }
        catch (Exception e){
            System.out.println("Error creating Produce. Please check your input.");
        }
    }

    /**
     * Adds a new Shellfish item to the inventory.
     * Asks the user to input information for the new Shellfish item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addShellfish() {
        try{
            System.out.println("Enter the name of the new Shellfish:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Shellfish (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Shellfish:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price per pound of the new Shellfish:");
            double pricePerPound = GetInput.getDoubleInput();
        
            System.out.println("Enter the weight of the new Shellfish:");
            double weight = GetInput.getDoubleInput();
        
            System.out.println("Does the new Shellfish have bones? (true/false):");
            boolean hasBones = GetInput.getBooleanInput();
        
            System.out.println("Enter the origin of the new Shellfish:");
            String origin = GetInput.getStringInput();
        
            System.out.println("Enter the type of the new Shellfish:");
            String type = GetInput.getStringInput();
        
            System.out.println("Does the new Shellfish have a shell? (true/false):");
            boolean hasShell = GetInput.getBooleanInput();
        
            Shellfish newShellfish = new Shellfish(getNextId(), name, expireDate, Validation.getAisleByCategory("SHELLFISH"), quantity, pricePerPound, weight, hasBones, origin, type, hasShell);
        
            items.add(newShellfish);
        
            System.out.println("New Shellfish added successfully.");
        }
        catch (Exception e){
            System.out.println("Error creating Shellfish. Please check your input.");
        }
    }
    
    /**
     * Adds a new Steak item to the inventory.
     * Asks the user to input information for the new Steak item, validates the input,
     * and adds the item to the list of all items.
     * @throws Exception   If an unexpected error occurs when creating new Item.
     */
    public void addSteak() {
        try{
            System.out.println("Enter the name of the new Steak:");
            String name = GetInput.getStringInput();
        
            System.out.println("Enter the expiration date of the new Steak (format: dd-MMM-yy):");
            String expireDate = GetInput.getDateInput();
        
            System.out.println("Enter the quantity of the new Steak:");
            int quantity = GetInput.getIntInput();
        
            System.out.println("Enter the price per pound of the new Steak:");
            double pricePerPound = GetInput.getDoubleInput();
        
            System.out.println("Enter the weight of the new Steak:");
            double weight = GetInput.getDoubleInput();
        
            System.out.println("Does the new Steak have bones? (true/false):");
            boolean hasBones = GetInput.getBooleanInput();
        
            System.out.println("Enter the cut of the new Steak:");
            String cut = GetInput.getStringInput();
        
            System.out.println("Enter the origin of the new Steak:");
            String origin = GetInput.getStringInput();
        
            System.out.println("Enter the source of the new Steak:");
            String source = GetInput.getStringInput();
        
            Steak newSteak = new Steak(getNextId(), name, expireDate, Validation.getAisleByCategory("STEAK"), quantity, pricePerPound, weight, origin, hasBones, cut, source);
        
            items.add(newSteak);
        
            System.out.println("New Steak added successfully.");
        }
        catch (Exception e){
            System.out.println("Error on creating Steak. Please check your input.");
        }
    }
}
