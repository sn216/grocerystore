package project.Commands;

import java.util.ArrayList;
import java.util.List;

import project.Item.Item;
import project.Util.GetInput;
import project.Util.Sorter;

/**
 * Represents a command that can be used to search for products that have a given name.
 */
public class SearchProductsByName extends Command {

    private List<Item> items;

    public SearchProductsByName(List<Item> items) {
        this.items = items;
    }

    @Override
    public String getCode() { return "sn"; }

    @Override
    public String getDescription() { return "Search items by name"; }

    @Override
    public void run() {
        System.out.println("Enter the name to search:");
        String name = GetInput.getStringInput();

        List<Item> nameItems = new ArrayList<>();

        for (Item item : items) {
            if (item.getName().toLowerCase().contains(name.toLowerCase())) {
                nameItems.add(item);
            }
        }

        if (nameItems.isEmpty()) {
            System.out.println("No items found for the specified name.");
            return;
        }

        List<Item> sortedItems = Sorter.promptNewSortedItemList(nameItems);

        System.out.println("\nHere are all the items we found:");
        System.out.println(Item.getItemTableHeading());
        for (Item item : sortedItems) {
            System.out.println(item);
        }
    }
}
