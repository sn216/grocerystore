package project; 

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import project.Item.Item;
import project.Util.LoadItems;
import project.Commands.*;
import project.Exceptions.ImportException;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime; 

public class Employee {
    private static List<Item> allItems = new ArrayList<>();
    public static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            allItems = LoadItems.loadItems();
            boolean exit = false;

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();

            System.out.println("\n                            Grocery Store Employee " + dtf.format(now));
            System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

            List<Command> commands = Arrays.asList(
                new ViewAllItems(allItems),
                new ViewAllDiscountedItems(allItems),
                new SearchProductsByName(allItems),
                new SearchProductsByCategory(allItems)
            );

            while (!exit) {
                System.out.println("");
                System.out.println("Which command would you like to run?");

                Iterator<Command> printIterator = commands.iterator();
                while (printIterator.hasNext()) {
                    Command command = printIterator.next();
                    System.out.println(command);
                }
                System.out.println("(e) Exit the program");

                String code = scan.nextLine().toLowerCase();
                Iterator<Command> iterator = commands.iterator();

                if (code.equals("e")) {
                    exit = true;
                    System.out.println("Exiting program.");
                    return;
                }

                // Check if any of the commands match the inputted code. If it does, run it.
                while (iterator.hasNext()) {
                    Command command = iterator.next();
                    if (command.getCode().equals(code)) {
                        command.run();
                        break;
                    }
                }
            }
        }
        catch (ImportException e) {
            System.out.println("Cannot load items. Application will exit.");
            e.printStackTrace();
        }
    }
}