package project.Util;

import java.util.Arrays;
import java.util.List;

import project.Item.Item;

public class Validation {

    /**
     * Given a string, checks if it's a valid sorting criteria (name, price, quantity) or not.
     * @param sortingCriteria The string to check
     * @return Whether or not it is valid.
     */
    public static boolean isValidSortingCriteria(String sortingCriteria) {
        switch (sortingCriteria) {
            case "PRICE":
            case "NAME":
            case "QUANTITY":
                return true;
            default:
                return false;
        }
    }

    /**
     * Checks if the provided category is a valid item category.
     * @param category The category to be checked.
     * @return true if the category is valid, false otherwise.
     */
    public static boolean isValidCategory(String category) {
        List<String> validCategories = Arrays.asList("bread", "cheese", "cookie", "fish", "milk", "pasta", "produce", "shellfish", "steak", "yogurt");
        return validCategories.contains(category);
    }

    /**
     * Checks if the given item belongs to the specified category.
     * The category is identified based on its string representation.
     * Valid category values include BREAD, CHEESE, COOKIE, FISH, MILK, PASTA, PRODUCE, SHELLFISH, STEAK, and YOGURT.
     * @param item The item to check against the specified category.
     * @param category The category string indicating the expected type of the item.
     * @return true if the item belongs to the specified category; false otherwise.
     */
    public static boolean isInstanceOfCategory(Item item, String category) {
        return item.getClass().getSimpleName().toLowerCase().equals(category.toLowerCase());
    }

    /**
     * Retrieves the aisle number associated with a specific category.
     * @param category The category for which the aisle number is requested.
     * @return The aisle number corresponding to the given category. Returns 0 for unknown categories.
     */
    public static int getAisleByCategory(String category) {
        switch (category.toUpperCase()) {
            case "BREAD":
                return 10;
            case "CHEESE":
                return 5;
            case "COOKIE":
                return 8;
            case "FISH":
                return 1;
            case "MILK":
                return 6;
            case "PASTA":
                return 9;
            case "PRODUCE":
                return 4;
            case "SHELLFISH":
                return 2;
            case "STEAK":
                return 3;
            case "YOGURT":
                return 7;
            default:
                return 0;
        }
    }
}
