package project.Util;

import java.util.Iterator;
import java.util.List;

import project.Discount.Discount;
import project.Exceptions.ImportException;
import project.Importer.Discounts.CouponImporter;
import project.Importer.Discounts.IDiscountImporter;
import project.Importer.Items.AllItemsImporter;
import project.Importer.Items.IProductImporter;
import project.Item.Item;

public class LoadItems {
    /**
     * Loads all items from the AllItemsImporter and then loads all discounts, which are applies to the items.
     * @return The list of items with discounts applied.
     * @throws ImportException If the items cannot be loaded.
     */
    public static List<Item> loadItems() throws ImportException {
        IProductImporter importer = new AllItemsImporter();
        List<Item> allItems = importer.loadItem();

        IDiscountImporter discountImporter = new CouponImporter();
        List<Discount> allCoupons = discountImporter.loadDiscount();

        Iterator<Discount> couponIterator = allCoupons.iterator();

        while (couponIterator.hasNext()) {
            Discount coupon = couponIterator.next();
            
            Iterator<Item> itemIterator = allItems.iterator();
            while (itemIterator.hasNext()) {
                Item item = itemIterator.next();
                if (item.getId() == coupon.getItemId()) {
                    item.setDiscount(coupon);
                }
            }
        }

        return allItems;
    }
}
