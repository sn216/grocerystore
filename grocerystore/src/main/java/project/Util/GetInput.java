package project.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GetInput {
    /**
     * Retrieves a valid non-negative integer input from the user.
     * @return An integer representing the valid non-negative input.
     * @throws NumberFormatException If an error occurs when using the wrong type of input.
     */
    public static int getIntInput() {
        while (true) {
            try {
                int input = Integer.parseInt(System.console().readLine());
                if (input >= 0) {
                    return input;
                } else {
                    System.out.println("Invalid input. Please enter a non-negative integer.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter an integer.");
            }
        }
    }
    
    /**
     * Retrieves a valid non-negative decimal number input from the user.
     * @return A double representing the valid non-negative decimal number input.
     * @throws NumberFormatException If an error occurs when using the wrong type of input.
     */
    public static double getDoubleInput() {
        while (true) {
            try {
                double input = Double.parseDouble(System.console().readLine());
                if (input >= 0) {
                    return input;
                } 
                else {
                    System.out.println("Invalid input. Please enter a non-negative decimal number.");
                }
            } 
            catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a decimal number.");
            }
        }
    }
    
    /**
     * Retrieves a string input from the user.
     * @return A string representing the user input.
     */
    public static String getStringInput() {
        return System.console().readLine();
    }
    
    /**
     * Retrieves a boolean input from the user.
     * @return true if the user input is 'true', false if 'false'.
     */
    public static boolean getBooleanInput() {
        while (true) {
            String input = System.console().readLine().toLowerCase();
            if ("true".equals(input) || "false".equals(input)) {
                return Boolean.parseBoolean(input);
            }
            System.out.println("Invalid input. Please enter true or false.");
        }
    }

    /**
     * Retrieves a valid category input from the user.
     * @return A string representing the valid category input.
     */
    public static String getCategoryInput() {
        while (true) {
            String input = System.console().readLine().toLowerCase();
            if (Validation.isValidCategory(input)) {
                return input;
            }
            System.out.println("Invalid category. Please enter a valid category.");
        }
    }

    public static boolean getYesNoInput() {
        while (true) {
            String input = System.console().readLine().toUpperCase();
            if ("Y".equals(input) || "N".equals(input)) {
                return "Y".equals(input);
            }
            System.out.println("Invalid input. Please enter Y or N.");
        }
    }

    /**
     * Retrieves a valid date input from the user in the format dd-MMM-yy.
     * @return A string representing the valid date input.
     */
    public static String getDateInput() {
        while (true) {
            try {
                System.out.println("Enter the date (format: dd-MMM-yy):");
                String input = System.console().readLine();
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
                dateFormat.setLenient(true);
                Date date = dateFormat.parse(input);
                
                if (date.before(new Date())) {
                    System.out.println("Invalid date. Please enter a date not before the current date.");
                    continue;
                }

                return dateFormat.format(date);
            } 
            catch (ParseException e) {
                e.printStackTrace();
                System.out.println("Invalid date format. Please enter a valid date.");
            }
        }
    }
}
