package project.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import project.Comparer.NameComparerAscending;
import project.Comparer.NameComparerDescending;
import project.Comparer.PriceComparerAscending;
import project.Comparer.PriceComparerDescending;
import project.Comparer.QuantityComparerAscending;
import project.Comparer.QuantityComparerDescending;
import project.Item.Item;

public class Sorter {
    /**
     * Given a list of items, this method will prompt the user with how to sort it.
     * First, it asks if they want to sort. If they say no, return the list as is.
     * If they want to sort, ask them if they want to sort by name, price or quantity.
     * Then, ask if they want to sort by ascending or descending.
     * @param items The list of items to sort
     * @return The sorted list of items.
     */
    public static List<Item> promptNewSortedItemList(List<Item> items) {
        List<Item> newSortedItems = new ArrayList<>();
        Iterator<Item> iterator = items.iterator();

        while (iterator.hasNext()) {
            Item item = iterator.next();
            newSortedItems.add(item);
        }

        System.out.println("Do you want to sort the list? (Y/N)");
        boolean sortOption = GetInput.getYesNoInput();

        if (sortOption) {
            String sortingCriteria;
            do {
                System.out.println("Enter the sorting criteria (PRICE/NAME/QUANTITY):");
                sortingCriteria = GetInput.getStringInput().toUpperCase();

                if (!Validation.isValidSortingCriteria(sortingCriteria)) {
                    System.out.println("Invalid sorting criteria.");
                }

            } while (!Validation.isValidSortingCriteria(sortingCriteria));

            System.out.println("Do you want to sort the list by ascending order? (Y/N)");
            boolean isAscending = GetInput.getYesNoInput();

            switch (sortingCriteria) {
                case "PRICE":
                    Comparator<Item> priceComparator = isAscending ? new PriceComparerAscending() : new PriceComparerDescending();
                    Collections.sort(newSortedItems, priceComparator);
                    break;
                case "NAME":
                    Comparator<Item> nameComparator = isAscending ? new NameComparerAscending() : new NameComparerDescending();
                    Collections.sort(newSortedItems, nameComparator);
                    break;
                case "QUANTITY":
                    Comparator<Item> quantityComparator = isAscending ? new QuantityComparerAscending() : new QuantityComparerDescending();
                    Collections.sort(newSortedItems, quantityComparator);
                    break;
                default:
                    System.out.println("Invalid sorting criteria. List will not be sorted.");
            }
        }

        return newSortedItems;
    }
}
