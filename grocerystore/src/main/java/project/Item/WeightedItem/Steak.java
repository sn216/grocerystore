package project.Item.WeightedItem;

public class Steak extends Meat {
    private String cut;
    private String source;

    /**
     * Constructs a Steak object with the specified attributes.
     * @param id            The unique identifier of the steak.
     * @param name          The name of the steak.
     * @param expireDate    The expiration date of the steak.
     * @param aisle         The aisle where the steak is located.
     * @param quantity      The quantity of the steak available.
     * @param pricePerPound The price per pound of the steak.
     * @param weight        The weight of the steak.
     * @param origin        The origin or source of the steak.
     * @param hasBones      Indicates whether the steak has bones or not.
     * @param cut           The cut or style of the steak.
     * @param source        The source or provider of the steak.
     */
    public Steak(int id, String name, String expireDate, int aisle, int quantity, double pricePerPound, double weight, String origin, boolean hasBones, String cut, String source) {
        super(id, name, expireDate, aisle, quantity, pricePerPound, weight, origin, hasBones);
        this.cut = cut;
        this.source = source;
    }

    /**
     * Gets the cut or style of the steak.
     * @return The cut or style of the steak.
     */
    public String getCut() {
        return this.cut;
    }

    /**
     * Gets the source or provider of the steak.
     * @return The source or provider of the steak.
     */
    public String getSource() {
        return this.source;
    }

    @Override
    public String toString() {
        return super.toString() + " Cut: " + this.cut + ", Source" + this.source;
    }
}
