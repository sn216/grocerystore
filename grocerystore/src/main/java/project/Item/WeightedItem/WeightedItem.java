package project.Item.WeightedItem;

import project.Item.Item;

public class WeightedItem extends Item {
    private double pricePerPound;
    private double weight;

    /**
     * Constructs a WeightedItem object with the specified attributes.
     * @param id            The unique identifier of the weighted item.
     * @param name          The name of the weighted item.
     * @param expireDate    The expiration date of the weighted item.
     * @param aisle         The aisle where the weighted item is located.
     * @param quantity      The quantity of the weighted item available.
     * @param pricePerPound The price per pound of the weighted item.
     * @param weight        The weight of the weighted item.
     * @throws IllegalArgumentException If weight is less than or equal to 0.
     */
    public WeightedItem(int id, String name, String expireDate, int aisle, int quantity, double pricePerPound, double weight) {
        super(id, name, expireDate, aisle, quantity);

        if (weight <= 0) throw new IllegalArgumentException("Weight cannot be greater or equal to 0.");

        this.setPricePerPound(pricePerPound);
        this.weight = weight;
    }

    @Override
    public double getRawPrice() {
        return this.pricePerPound * this.weight;
    }

    @Override
    public double getPrice() {
        if (this.hasDiscount()) {
            return this.getDiscount().getDiscountedPrice(this.getRawPrice());
        }
        else {
            return this.getRawPrice();
        }
    }

    /**
     * Sets the price per pound of the weighted item.
     * @param newPricePerPound The new price per pound of the weighted item.
     * @throws IllegalArgumentException If newPricePerPound is less than or equal to 0.
     */
    public void setPricePerPound(double newPricePerPound) {
        if (newPricePerPound <= 0) throw new IllegalArgumentException("Price per pound cannot be lower or equal to 0.");
        this.pricePerPound = newPricePerPound;
    }

    /**
     * Gets the price per pound of the weighted item.
     * @return The price per pound of the weighted item.
     */
    public double getPricePerPound() {
        return this.pricePerPound;
    }

    /**
     * Gets the weight of the weighted item.
     * @return The weight of the weighted item.
     */
    public double getWeight() {
        return this.weight;
    }
}
