package project.Item.WeightedItem;

public class Shellfish extends Seafood {
    private boolean hasShell;

    /**
     * Constructs a Shellfish object with the specified attributes.
     * @param id            The unique identifier of the shellfish.
     * @param name          The name of the shellfish.
     * @param expireDate    The expiration date of the shellfish.
     * @param aisle         The aisle where the shellfish is located.
     * @param quantity      The quantity of the shellfish available.
     * @param pricePerPound The price per pound of the shellfish.
     * @param weight        The weight of the shellfish.
     * @param origin        The origin or source of the shellfish.
     * @param hasBones      Indicates whether the shellfish has bones or not.
     * @param type          The type or species of the shellfish.
     * @param hasShell      Indicates whether the shellfish has a shell or not.
     */
    public Shellfish(int id, String name, String expireDate, int aisle, int quantity, double pricePerPound, double weight, boolean hasBones, String origin, String type, boolean hasShell) {
        super(id, name, expireDate, aisle, quantity, pricePerPound, weight, origin, hasBones, type);
        this.hasShell = hasShell;
    }

    /**
     * Checks if the shellfish has a shell.
     * @return True if the shellfish has a shell, false otherwise.
     */
    public boolean hasShell() {
        return this.hasShell;
    }

    @Override
    public String toString() {
        return super.toString() + " HasShell: " + this.hasShell;
    }
}
