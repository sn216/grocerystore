package project.Item.WeightedItem;

public class Fish extends Seafood {
    private boolean hasScales;
    private String cut;

    /**
     * Constructs a Fish object with the specified attributes.
     * @param id            The unique identifier of the fish.
     * @param name          The name of the fish.
     * @param expireDate    The expiration date of the fish.
     * @param aisle         The aisle where the fish is located.
     * @param quantity      The quantity of the fish available.
     * @param pricePerPound The price per pound of the fish.
     * @param weight        The weight of the fish.
     * @param origin        The origin or source of the fish.
     * @param hasBones      Indicates whether the fish has bones or not.
     * @param type          The type or species of the fish.
     * @param cut           The cut or preparation of the fish.
     * @param hasScales     Indicates whether the fish has scales or not.
     */
    public Fish(int id, String name, String expireDate, int aisle, int quantity, double pricePerPound, double weight, String origin, boolean hasBones, String type, String cut, boolean hasScales) {
        super(id, name, expireDate, aisle, quantity, pricePerPound, weight, origin, hasBones, type);
        this.hasScales = hasScales;
        this.cut = cut;
    }

    /**
     * Checks if the fish has scales.
     * @return True if the fish has scales, false otherwise.
     */
    public boolean hasScales() {
        return this.hasScales;
    }

    /**
     * Gets the cut or preparation of the fish.
     * @return The cut or preparation of the fish.
     */
    public String getCut() {
        return this.cut;
    }
    
    @Override
    public String toString() {
        return super.toString() + " HasScales: " + this.hasScales + ", Cut: " + this.cut;
    }
}
