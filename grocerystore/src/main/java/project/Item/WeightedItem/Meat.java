package project.Item.WeightedItem;

public class Meat extends WeightedItem {
    private boolean hasBones;
    private String origin;

    /**
     * Constructs a Meat object with the specified attributes.
     * @param id            The unique identifier of the meat.
     * @param name          The name of the meat.
     * @param expireDate    The expiration date of the meat.
     * @param aisle         The aisle where the meat is located.
     * @param quantity      The quantity of the meat available.
     * @param pricePerPound The price per pound of the meat.
     * @param weight        The weight of the meat.
     * @param origin        The origin or source of the meat.
     * @param hasBones      Indicates whether the meat has bones or not.
     */
    public Meat(int id, String name, String expireDate, int aisle, int quantity, double pricePerPound, double weight, String origin, boolean hasBones) {
        super(id, name, expireDate, aisle, quantity, pricePerPound, weight);
        this.hasBones = hasBones;
        this.origin = origin;
    }

    /**
     * Checks if the meat has bones.
     * @return True if the meat has bones, false otherwise.
     */
    public boolean hasBones() {
        return this.hasBones;
    }

    /**
     * Gets the origin or source of the meat.
     * @return The origin or source of the meat.
     */
    public String getOrigin() {
        return this.origin;
    }

    @Override
    public String toString() {
        return super.toString() + " HasBones: " + this.hasBones + ", Origin: " + this.origin + ",";
    }
}

