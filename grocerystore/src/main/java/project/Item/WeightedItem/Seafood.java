package project.Item.WeightedItem;

public class Seafood extends Meat {
    private String type;

    /**
     * Constructs a Seafood object with the specified attributes.
     * @param id            The unique identifier of the seafood.
     * @param name          The name of the seafood.
     * @param expireDate    The expiration date of the seafood.
     * @param aisle         The aisle where the seafood is located.
     * @param quantity      The quantity of the seafood available.
     * @param pricePerPound The price per pound of the seafood.
     * @param weight        The weight of the seafood.
     * @param origin        The origin or source of the seafood.
     * @param hasBones      Indicates whether the seafood has bones or not.
     * @param type          The type or species of the seafood.
     */
    public Seafood(int id, String name, String expireDate, int aisle, int quantity, double pricePerPound, double weight, String origin, boolean hasBones, String type) {
        super(id, name, expireDate, aisle, quantity, pricePerPound, weight, origin, hasBones);
        this.type = type;
    }

    /**
     * Gets the type or species of the seafood.
     * @return The type or species of the seafood.
     */
    public String getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return super.toString() + " Type: " + this.type + ",";
    }
}
