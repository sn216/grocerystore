package project.Item.WeightedItem;

public class Produce extends WeightedItem {
    private boolean hasSeeds;
    private String origin;

    /**
     * Constructs a Produce object with the specified attributes.
     * @param id            The unique identifier of the produce.
     * @param name          The name of the produce.
     * @param expireDate    The expiration date of the produce.
     * @param aisle         The aisle where the produce is located.
     * @param quantity      The quantity of the produce available.
     * @param pricePerPound The price per pound of the produce.
     * @param weight        The weight of the produce.
     * @param origin        The origin or source of the produce.
     * @param hasSeeds      Indicates whether the produce has seeds or not.
     */
    public Produce(int id, String name, String expireDate, int aisle, int quantity, double pricePerPound, double weight, String origin, boolean hasSeeds) {
        super(id, name, expireDate, aisle, quantity, pricePerPound, weight);
        this.hasSeeds = hasSeeds;
        this.origin = origin;
    }

    /**
     * Checks if the produce has seeds.
     * @return True if the produce has seeds, false otherwise.
     */
    public boolean hasSeeds() {
        return this.hasSeeds;
    }

    /**
     * Gets the origin or source of the produce.
     * @return The origin or source of the produce.
     */
    public String getOrigin() {
        return this.origin;
    }

    @Override
    public String toString() {
        return super.toString() + " HasSeeds: " + this.hasSeeds + ", Origin: " + this.origin;
    }
}
