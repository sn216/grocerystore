package project.Item.UnweightedItem;

public class Cookie extends UnweightedItem {
    private String ingredient;

    /**
     * Constructs a Cookie object with the specified attributes.
     * @param id         The unique identifier of the cookie.
     * @param name       The name of the cookie.
     * @param expireDate The expiration date of the cookie.
     * @param aisle      The aisle where the cookie is located.
     * @param quantity   The quantity of the cookie available.
     * @param price      The price of the cookie.
     * @param ingredient The main ingredient of the cookie.
     */
    public Cookie(int id, String name, String expireDate, int aisle, int quantity, double price, String ingredient) {
        super(id, name, expireDate, aisle, quantity, price);
        this.ingredient = ingredient;
    }

    /**
     * Gets the main ingredient of the cookie.
     * @return The main ingredient of the cookie.
     */
    public String getIngredient() {
        return this.ingredient;
    }

    @Override
    public String toString() {
        return super.toString() + " Ingredient: " + this.ingredient;
    }
}
