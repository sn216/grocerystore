package project.Item.UnweightedItem;

public class Dairy extends UnweightedItem {
    private String source;
    private boolean isVegan;

    /**
     * Constructs a Dairy object with the specified attributes.
     * @param id         The unique identifier of the dairy item.
     * @param name       The name of the dairy item.
     * @param expireDate The expiration date of the dairy item.
     * @param aisle      The aisle where the dairy item is located.
     * @param quantity   The quantity of the dairy item available.
     * @param price      The price of the dairy item.
     * @param source     The source of the dairy item (e.g., cow's milk).
     * @param isVegan    Indicates whether the dairy item is vegan or not.
     */
    public Dairy(int id, String name, String expireDate, int aisle, int quantity, double price, String source, boolean isVegan) {
        super(id, name, expireDate, aisle, quantity, price);
        this.source = source;
        this.isVegan = isVegan;
    }

    /**
     * Gets the source of the dairy item.
     * @return The source of the dairy item (e.g., cow's milk).
     */
    public String getSource() {
        return source;
    }

    /**
     * Checks if the dairy item is vegan.
     * @return True if the dairy item is vegan, false otherwise.
     */
    public boolean getIsVegan() {
        return this.isVegan;
    }

    @Override
    public String toString() {
        return super.toString() + " Source: " + this.source + ", Vegan: " + this.isVegan + ",";
    }
}
