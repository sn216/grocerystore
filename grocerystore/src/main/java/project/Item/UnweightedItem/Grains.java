package project.Item.UnweightedItem;

public class Grains extends UnweightedItem {
    private boolean wholeWheat;

    /**
     * Constructs a Grains object with the specified attributes.
     * @param id         The unique identifier of the grains item.
     * @param name       The name of the grains item.
     * @param expireDate The expiration date of the grains item.
     * @param aisle      The aisle where the grains item is located.
     * @param quantity   The quantity of the grains item available.
     * @param price      The price of the grains item.
     * @param wholeWheat Indicates whether the grains item is whole wheat or not.
     */
    public Grains(int id, String name, String expireDate, int aisle, int quantity, double price, boolean wholeWheat) {
        super(id, name, expireDate, aisle, quantity, price);
        this.wholeWheat = wholeWheat;
    }

    /**
     * Checks if the grains item is whole wheat.
     * @return True if the grains item is whole wheat, false otherwise.
     */
    public boolean isWholeWheat() {
        return wholeWheat;
    }

    @Override
    public String toString() {
        return super.toString() + " WholeWheat: " + this.wholeWheat + ",";
    }
}
