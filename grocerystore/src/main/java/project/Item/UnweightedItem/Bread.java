package project.Item.UnweightedItem;

public class Bread extends Grains {
    private boolean sliced;

    /**
     * Constructs a Bread object with the specified attributes.
     * @param id         The unique identifier of the bread.
     * @param name       The name of the bread.
     * @param expireDate The expiration date of the bread.
     * @param aisle      The aisle where the bread is located.
     * @param quantity   The quantity of the bread available.
     * @param price      The price of the bread.
     * @param wholeWheat Indicates whether the bread is whole wheat or not.
     * @param sliced     Indicates whether the bread is sliced or not.
     */
    public Bread(int id, String name, String expireDate, int aisle, int quantity, double price, boolean wholeWheat, boolean sliced) {
        super(id, name, expireDate, aisle, quantity, price, wholeWheat);
        this.sliced = sliced;
    }

    /**
     * Gets the slicing status of the bread.
     * @return True if the bread is sliced, false otherwise.
     */
    public boolean getIsSliced() {
        return sliced;
    }

    @Override
    public String toString() {
        return super.toString() + " Sliced: " + this.sliced;
    }
}
