package project.Item.UnweightedItem;

public class Milk extends Dairy {
    private double percentage;
    private double volume;
    private String packaging;

    /**
     * Constructs a Milk object with the specified attributes.
     * @param id         The unique identifier of the milk.
     * @param name       The name of the milk.
     * @param expireDate The expiration date of the milk.
     * @param aisle      The aisle where the milk is located.
     * @param quantity   The quantity of the milk available.
     * @param price      The price of the milk.
     * @param source     The source of the milk (e.g., cow's milk).
     * @param isVegan    Indicates whether the milk is vegan or not.
     * @param percentage The percentage of fat in the milk.
     * @param volume     The volume of the milk.
     * @param packaging  The packaging type of the milk.
     */
    public Milk(int id, String name, String expireDate, int aisle, int quantity, double price, String source, boolean isVegan, double percentage, double volume, String packaging) {
        super(id, name, expireDate, aisle, quantity, price, source, isVegan);
        this.percentage = percentage;
        this.volume = volume;
        this.packaging = packaging;
    }

    /**
     * Gets the percentage of fat in the milk.
     * @return The percentage of fat in the milk.
     */
    public double getPercentage() {
        return percentage;
    }

    /**
     * Gets the volume of the milk.
     * @return The volume of the milk.
     */
    public double getVolume() {
        return volume;
    }

    /**
     * Gets the packaging type of the milk.
     * @return The packaging type of the milk.
     */
    public String getPackaging() {
        return packaging;
    }

    @Override
    public String toString() {
        return super.toString() + " Percentage: " + this.percentage + ", Volume: " + this.volume + ", Packaging: " + this.packaging;
    }
}


