package project.Item.UnweightedItem;

public class Yogurt extends Dairy {
    private String flavor;

    /**
     * Constructs a Yogurt object with the specified attributes.
     * @param id         The unique identifier of the yogurt.
     * @param name       The name of the yogurt.
     * @param expireDate The expiration date of the yogurt.
     * @param aisle      The aisle where the yogurt is located.
     * @param quantity   The quantity of the yogurt available.
     * @param price      The price of the yogurt.
     * @param source     The source of the yogurt (e.g., cow's milk).
     * @param isVegan    Indicates whether the yogurt is vegan or not.
     * @param flavor     The flavor of the yogurt.
     */
    public Yogurt(int id, String name, String expireDate, int aisle, int quantity, double price, String source, boolean isVegan, String flavor) {
        super(id, name, expireDate, aisle, quantity, price, source, isVegan);
        this.flavor = flavor;
    }

    /**
     * Gets the flavor of the yogurt.
     * @return The flavor of the yogurt.
     */
    public String getFlavor() {
        return flavor;
    }

    @Override
    public String toString() {
        return super.toString() + " Flavor: " + this.flavor;
    }
}