package project.Item.UnweightedItem;

public class Pasta extends Grains {
    private String type;

    /**
     * Constructs a Pasta object with the specified attributes.
     * @param id         The unique identifier of the pasta.
     * @param name       The name of the pasta.
     * @param expireDate The expiration date of the pasta.
     * @param aisle      The aisle where the pasta is located.
     * @param quantity   The quantity of the pasta available.
     * @param price      The price of the pasta.
     * @param wholeWheat Indicates whether the pasta is whole wheat or not.
     * @param type       The type or variety of the pasta.
     */
    public Pasta(int id, String name, String expireDate, int aisle, int quantity, double price, boolean wholeWheat, String type) {
        super(id, name, expireDate, aisle, quantity, price, wholeWheat);
        this.type = type;
    }

    /**
     * Gets the type or variety of the pasta.
     * @return The type or variety of the pasta.
     */
    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return super.toString() + " Type: " + this.type;
    }
}
