package project.Item.UnweightedItem;

import project.Item.Item;

public class UnweightedItem extends Item {
    private double price;

    /**
     * Constructs an UnweightedItem object with the specified attributes.
     * @param id         The unique identifier of the unweighted item.
     * @param name       The name of the unweighted item.
     * @param expireDate The expiration date of the unweighted item.
     * @param aisle      The aisle where the unweighted item is located.
     * @param quantity   The quantity of the unweighted item available.
     * @param price      The price of the unweighted item.
     */
    public UnweightedItem(int id, String name, String expireDate, int aisle, int quantity, double price) {
        super(id, name, expireDate, aisle, quantity);
        this.setPrice(price);
    }

    @Override
    public double getRawPrice() {
        return price;
    }

    @Override
    public double getPrice() {
        if (this.hasDiscount()) {
            return this.getDiscount().getDiscountedPrice(this.getRawPrice());
        }
        else {
            return this.getRawPrice();
        }
    }

    /**
     * Sets the price of the unweighted item.
     * @param newPrice The new price to be set.
     * @throws IllegalArgumentException if the new price is lower or equal to 0.
     */
    public void setPrice(double newPrice) {
        if (newPrice <= 0) throw new IllegalArgumentException("Price cannot be lower or equal to 0.");
        this.price = newPrice;
    }
}
