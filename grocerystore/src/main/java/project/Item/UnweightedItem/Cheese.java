package project.Item.UnweightedItem;

public class Cheese extends Dairy {
    private double timeAged;

    /**
     * Constructs a Cheese object with the specified attributes.
     * @param id         The unique identifier of the cheese.
     * @param name       The name of the cheese.
     * @param expireDate The expiration date of the cheese.
     * @param aisle      The aisle where the cheese is located.
     * @param quantity   The quantity of the cheese available.
     * @param price      The price of the cheese.
     * @param source     The source of the cheese (e.g., cow's milk).
     * @param isVegan    Indicates whether the cheese is vegan or not.
     * @param timeAged   The time for which the cheese has been aged.
     */
    public Cheese(int id, String name, String expireDate, int aisle, int quantity, double price, String source, boolean isVegan, double timeAged) {
        super(id, name, expireDate, aisle, quantity, price, source, isVegan);
        this.timeAged = timeAged;
    }

    /**
     * Gets the time for which the cheese has been aged.
     * @return The time (in some unit) for which the cheese has been aged.
     */
    public double getTimeAged() {
        return this.timeAged;
    }

    @Override
    public String toString() {
        return super.toString() + " TimeAged: " + this.timeAged;
    }
}
