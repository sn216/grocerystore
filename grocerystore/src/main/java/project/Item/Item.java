package project.Item;

import java.text.DecimalFormat;

import project.Discount.Discount;

public abstract class Item {
    private int id;
    private String name;
    private String expireDate;
    private int aisle;
    private int quantity;

    private Discount discount;

    /**
     * Constructs an Item object with the specified attributes.
     * @param id          The unique identifier of the item.
     * @param name        The name of the item.
     * @param expireDate  The expiration date of the item.
     * @param aisle       The aisle where the item is located.
     * @param quantity    The quantity of the item.
     * @throws IllegalArgumentException If id is less than 0 or aisle is less than 1.
     */
    public Item(int id, String name, String expireDate, int aisle, int quantity) {
        if (id < 0) throw new IllegalArgumentException("Id cannot be lower than 0.");
        if (aisle < 1) throw new IllegalArgumentException("Aisle cannot be lower than 1.");

        this.id = id;
        this.name = name;
        this.expireDate = expireDate;
        this.aisle = aisle;
        this.setQuantity(quantity);
    }

    /**
     * Gets the price of the item.
     * @return The price of the item.
     */
    public abstract double getPrice();
    
    public abstract double getRawPrice();

    /**
     * Gets the unique identifier of the item.
     * @return The unique identifier of the item.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Gets the name of the item.
     * @return The name of the item.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the expiration date of the item.
     * @return The expiration date of the item.
     */
    public String getExpireDate() {
        return this.expireDate;
    }

    /**
     * Gets the aisle where the item is located.
     * @return The aisle where the item is located.
     */
    public int getAisle() {
        return this.aisle;
    }

    /**
     * Gets the quantity of the item.
     * @return The quantity of the item.
     */
    public int getQuantity() {
        return this.quantity;
    }

    /**
     * Sets the quantity of the item.
     * @param newQuantity The new quantity of the item.
     * @throws IllegalArgumentException If newQuantity is less than 0.
     */
    public void setQuantity(int newQuantity) {
        if (newQuantity < 0) throw new IllegalArgumentException("Quantity cannot be lower than 0.");
        this.quantity = newQuantity;
    }

    public boolean hasDiscount() {
        return !(this.discount == null);
    }

    public Discount getDiscount() {
        return this.discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    /**
     * Returns a string representation of the Item object.
     * @return A string containing the details of the item.
     */
    @Override
    public String toString() {
        // %-Xs = pad the string to take up X amount of characters (left aligned)
        // %Xs = pad the string to take up X amount of characters (right aligned)
        return String.format(
            "%-6s%-28s%-12s%8s%9s%8s  %-8s       ",
            this.getId(),
            this.getName(),
            this.getExpireDate(),
            this.getAisle(), 
            this.getQuantity(),
            new DecimalFormat("0.00").format(this.getRawPrice()),
            this.hasDiscount() ? new DecimalFormat("0.00").format(this.getPrice()) : "n/a"
        );
    }

    /**
     * Returns a string that can be placed as a table heading above a table of items.
     * @return Heading string.
     */
    public static String getItemTableHeading() {
        return String.format(
            "%-6s%-28s%-12s%8s%9s%8s  %-16s%-20s",
            "Id",
            "Name",
            "Expire Date",
            "Aisle",
            "Quantity",
            "Price",
            "Discount Price",
            "Extra Information"
        );
    }
}
