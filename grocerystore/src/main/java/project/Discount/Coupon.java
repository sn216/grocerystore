package project.Discount;

public class Coupon implements Discount {
    private int id;
    private double percentage;
    private int itemId;

    /**
     * Constructs a Coupon with the specified ID, percentage discount, and associated item ID.
     * @param id         The unique identifier for the coupon.
     * @param percentage The percentage discount offered by the coupon.
     * @param itemId     The ID of the item to which the coupon is applicable.
     */
    public Coupon(int id, double percentage, int itemId) {
        this.id = id;
        this.percentage = percentage;
        this.itemId = itemId;
    }

    /**
     * Gets the ID of the coupon.
     * @return The ID of the coupon.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Gets the percentage discount offered by the coupon.
     * @return The percentage discount.
     */
    public double getPercentage() {
        return this.percentage;
    }

    @Override
    public int getItemId() {
        return this.itemId;
    }

    @Override
    public double getDiscountedPrice(double originalPrice) {
        return originalPrice * (1 - (this.percentage / 100));
    }

    @Override
    public String toString() {
        return String.format(
            "%-6s%-12s%-12s%-8s",
            this.id,
            "Coupon",
            this.percentage + "% off",
            this.itemId
        );
    }
}

