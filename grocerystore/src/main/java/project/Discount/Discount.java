package project.Discount;

public interface Discount {
    /**
     * Calculates the discounted price based on the original price of an item.
     * @param originalPrice The original price of the item.
     * @return The discounted price after applying the discount.
     */
    double getDiscountedPrice(double originalPrice); 

    /**
     * Gets the id of the item that this discount is associated to.
     * @return The item id associated to this discount.
     */
    int getItemId();
}
