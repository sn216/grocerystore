package project.Exceptions;

public class ImportException extends Exception {
    /**
     * Exception that will be thrown when an item or discount is unable to be fetched from its external source.
     */
    public ImportException(Throwable cause) {
        super(cause);
    }
}
