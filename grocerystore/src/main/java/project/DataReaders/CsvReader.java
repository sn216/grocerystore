package project.DataReaders;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import project.Exceptions.ImportException;

public class CsvReader implements IDataReader {
    private String filePath;

    /**
     * Constructs a CsvReader with the specified file path.
     * @param filePath The path to the CSV file.
     */
    public CsvReader(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public List<List<String>> getData() throws ImportException {
        Path p = Paths.get(this.filePath);
        List<String> lines;
        try {
            lines = Files.readAllLines(p);
        }
        catch (IOException e) {
            throw new ImportException(e);
        }
        
        List<List<String>> rows = new ArrayList<List<String>>();

        for (int i = 1; i < lines.size(); i++) {
            List<String> fields = new ArrayList<String>(Arrays.asList(lines.get(i).split(",")));
            rows.add(fields);
        }

        return rows;
    }
}

