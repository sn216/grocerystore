package project.DataReaders;

import java.util.List;

import project.Exceptions.ImportException;

public interface IDataReader {

    /**
     * Gets the data from the data source and returns it as a list of rows, where each row is represented as a list of strings.
     * @return A list of rows, where each row is represented as a list of strings.
     * @throws ImportException If an error occurs while retrieving the data.
     */
    List<List<String>> getData() throws ImportException;
}
