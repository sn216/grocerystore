package project.DataReaders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import project.Exceptions.ImportException;

public class SqlReader implements IDataReader {
    private String sqlString;
    private Connection conn;

    /**
     * Constructs a SqlReader with the specified SQL query and database connection.
     * @param sqlString The SQL query to execute.
     * @param conn      The database connection to use.
     */
    public SqlReader(String sqlString, Connection conn) {
        this.sqlString = sqlString;
        this.conn = conn;
    }

    @Override
    public List<List<String>> getData() throws ImportException {
        try {
            PreparedStatement stmt = conn.prepareStatement(sqlString);
            ResultSet results = stmt.executeQuery();
            ResultSetMetaData resultsMetadata = results.getMetaData();

            List<List<String>> rows = new ArrayList<List<String>>();

            while (results.next()) {
                List<String> fields = new ArrayList<String>();
                for (int i = 1; i <= resultsMetadata.getColumnCount(); i++) {
                    fields.add(results.getString(i));
                }
                rows.add(fields);
            }

            return rows;
        }
        catch (SQLException e) {
            throw new ImportException(e);
        }
    }
}

