package project.Importer.Items;

import java.util.List;

import project.Exceptions.ImportException;
import project.Item.Item;

public interface IProductImporter {

    /**
     * Loads and returns a list of items from a data source.
     * @return A list of Item objects representing the imported products.
     * @throws Exception If an error occurs during the process of loading items.
     */
    List<Item> loadItem() throws ImportException;
}
