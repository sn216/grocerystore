package project.Importer.Items;

import java.util.ArrayList;
import java.util.List;
import project.DataReaders.CsvReader;
import project.Exceptions.ImportException;
import project.Item.*;
import project.Item.UnweightedItem.Yogurt;

public class YogurtImporter implements IProductImporter {

    /**
     * Loads and returns a list of yogurt items from a CSV file.
     * @return A list of Item objects representing the imported yogurt items.
     * @throws ImportException If an I/O error occurs while reading the CSV file.
     */
    @Override
    public List<Item> loadItem() throws ImportException {
        List<Item> itemList = new ArrayList<Item>();
        CsvReader reader = new CsvReader("./resources/Yogurt.csv");
        // SqlReader implementation
        // SqlReader reader = new SqlReader("SELECT * FROM Yogurt", DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca", "USERNAME", "PASSWORD"));
        List<List<String>> csvData = reader.getData();
        
        for (int i = 0; i < csvData.size(); i++) {
            List<String> pieces = csvData.get(i);
            Item item = new Yogurt(
                Integer.parseInt(pieces.get(0)),
                pieces.get(1),
                pieces.get(2),
                Integer.parseInt(pieces.get(3)),
                Integer.parseInt(pieces.get(4)),
                Double.parseDouble(pieces.get(5)),
                pieces.get(6),
                Boolean.parseBoolean(pieces.get(7)),
                pieces.get(8)
            );

            itemList.add(item);
        }

        return itemList;
    }
}