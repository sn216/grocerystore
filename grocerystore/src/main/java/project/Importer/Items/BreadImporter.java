package project.Importer.Items;

import java.util.ArrayList;
import java.util.List;
import project.DataReaders.CsvReader;
import project.Exceptions.ImportException;
import project.Item.Item;
import project.Item.UnweightedItem.Bread;

public class BreadImporter implements IProductImporter {

    /**
     * Loads and returns a list of bread items from a CSV file.
     * @return A list of Item objects representing the imported bread items.
     * @throws ImportException If an I/O error occurs while reading the CSV file.
     */
    @Override
    public List<Item> loadItem() throws ImportException {
        List<Item> itemList = new ArrayList<Item>();
        CsvReader reader = new CsvReader("./resources/Bread.csv");
        List<List<String>> csvData = reader.getData();

        for (int i = 0; i < csvData.size(); i++) {
            List<String> pieces = csvData.get(i);
            Item item = new Bread(
                    Integer.parseInt(pieces.get(0)),
                    pieces.get(1),
                    pieces.get(2),
                    Integer.parseInt(pieces.get(3)),
                    Integer.parseInt(pieces.get(4)),
                    Double.parseDouble(pieces.get(5)),
                    Boolean.parseBoolean(pieces.get(6)),
                    Boolean.parseBoolean(pieces.get(7))
            );

            itemList.add(item);
        }

        return itemList;
    }
}

