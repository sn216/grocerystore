package project.Importer.Items;

import java.util.ArrayList;
import java.util.List;
import project.DataReaders.CsvReader;
import project.Exceptions.ImportException;
import project.Item.Item;
import project.Item.UnweightedItem.Cookie;

public class CookieImporter implements IProductImporter {

    /**
     * Loads and returns a list of cookie items from a CSV file.
     * @return A list of Item objects representing the imported cookie items.
     * @throws ImportException If an I/O error occurs while reading the CSV file.
     */
    @Override
    public List<Item> loadItem() throws ImportException {
        List<Item> itemList = new ArrayList<Item>();
        CsvReader reader = new CsvReader("./resources/Cookie.csv");
        List<List<String>> csvData = reader.getData();

        for (int i = 0; i < csvData.size(); i++) {
            List<String> pieces = csvData.get(i);
            Item item = new Cookie(
                    Integer.parseInt(pieces.get(0)),
                    pieces.get(1),
                    pieces.get(2),
                    Integer.parseInt(pieces.get(3)),
                    Integer.parseInt(pieces.get(4)),
                    Double.parseDouble(pieces.get(5)),
                    pieces.get(6)
            );

            itemList.add(item);
        }

        return itemList;
    }
}
