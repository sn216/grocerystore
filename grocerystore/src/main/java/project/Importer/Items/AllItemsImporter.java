package project.Importer.Items;

import java.util.ArrayList;
import java.util.List;

import project.Exceptions.ImportException;
import project.Item.Item;

public class AllItemsImporter implements IProductImporter {
    private List<IProductImporter> importers;

    public AllItemsImporter() {
        this.importers = new ArrayList<>();
        importers.add(new BreadImporter());
        importers.add(new CheeseImporter());
        importers.add(new CookieImporter());
        importers.add(new FishImporter());
        importers.add(new MilkImporter());
        importers.add(new PastaImporter());
        importers.add(new ProduceImporter());
        importers.add(new ShellfishImporter());
        importers.add(new SteakImporter());
        importers.add(new YogurtImporter());
    }

    /**
     * Loads and returns a list of all items by aggregating items loaded by individual product importers.
     * @return A list of Item objects representing all imported items.
     * @throws ImportException If an I/O error occurs during the item loading process.
     */
    @Override
    public List<Item> loadItem() {
        List<Item> allItems = new ArrayList<>();

        for (IProductImporter importer : importers) {
            try {
                List<Item> importedItems = importer.loadItem();
                allItems.addAll(importedItems);
            } catch (ImportException e) {
                e.printStackTrace();
            }
        }

        return allItems;
    }
}



