package project.Importer.Discounts;

import java.util.List;
import project.Discount.Discount;
import project.Exceptions.ImportException;

public interface IDiscountImporter  {

    /**
     * Loads and returns a list of discounts.
     * @return A list of Discount objects representing the imported discounts.
     * @throws Exception If an error occurs during the process of loading discounts.
     */
    List<Discount> loadDiscount() throws ImportException;
} 
