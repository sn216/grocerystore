package project.Importer.Discounts;

import java.util.ArrayList;
import java.util.List;

import project.DataReaders.CsvReader;
import project.Discount.Discount;
import project.Exceptions.ImportException;
import project.Discount.Coupon;

public class CouponImporter implements IDiscountImporter {

    /**
     * Loads and returns a list of coupons from a CSV file.
     * @return A list of Discount objects representing coupons.
     * @throws ImportException If an I/O error occurs while reading the CSV file.
     */
    @Override
    public List<Discount> loadDiscount() throws ImportException {
        List<Discount> couponList = new ArrayList<Discount>();
        CsvReader reader = new CsvReader("./resources/Coupon.csv");
        List<List<String>> csvData = reader.getData();

        for (int i = 0; i < csvData.size(); i++) {
            List<String> pieces = csvData.get(i);
            Coupon coupon = new Coupon(
                    Integer.parseInt(pieces.get(0)),
                    Double.parseDouble(pieces.get(1)),
                    Integer.parseInt(pieces.get(2))
            );

            couponList.add(coupon);
        }

        return couponList;
    }
}
